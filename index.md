# DevoOoups
agile tour bordeaux

Luc Mazardo

2 novembre 2018

:::notes
DevoOoups

Les containeurs, les orchestrateurs, les provisionners, le continous delivery, beaucoup d'outils sont disponibles pour faire du devops.
- A qui sont-ils destinés ?
- Mais qu'est-ce vraiment le mouvement devops tant à la mode ?

De temps en temps développeur, parfois scrummaster, administrateur système en apprentissage, passionné par l'open source, j'espère par cette intervention clarifier le mot devops. Mots clés : docker, k8s, ansible, continuous delivery, devops.
:::

# {data-background="img/tentacle.png"} 
* développeur ;
* parfois ScrumMaster ;
* administrateur système (en apprentissage) ;
* passionné par l'**open source**.

# et vous ?
> - y a-t-il des développeurs dans la salle ?
> - des administrateurs système ?
> - des devops ?

# c'est quoi un devops ? {data-background="img/404-yak.png"}

# les outils {data-background="img/devops_tools.png"}

# La tendance
![](img/devops_google_trends.png){width=80% height=80%}

:::notes
novembre 2016
:::

^[source google trends](https://trends.google.fr/trends/explore?date=all&q=%2Fm%2F0c3tq11,%2Fm%2F018j6p,%2Fm%2F0148v7)^

#

![](img/comparaison_google_trends.png){width=80% height=80%}

^[source google trends](https://trends.google.fr/trends/explore?date=all&q=%2Fm%2F0c3tq11,%2Fm%2F018j6p,%2Fm%2F0148v7)^

# Intitulés de postes

> - Ingénieur DevOps junior ou expérimenté
> - DevOps/ Administrateur Jenkins
> - Développeur confirmé Javascript Fullstack/Devops
> - Concepteur d'Applications web Java Devops
> - Responsable équipe DevOps
> - Lead DevOps
> - DevOps orienté Java / Shell Unix
> - Développeur Symfony et technicien DevOps

# Les compétences recherchées {data-background="img/devops_keywords.png"}

# Wanted

un jedi du code **ET** un ninja de l'administrateur système

# Le salaire
commençant à 30 k€ et pouvant dépasser les 90 k€

> - négociable 

# Les formations

<span style="background:grey">Professionnelles</span>

DevOps : Formation Kubernetes et les Microservices

Docker Ansible : Container DevOps

<span style="background:grey">Universitaires</span>

Developpeur d’applications d’entreprise, administrateur de S.I.

Web, E-Commerce et Big Data

# devops en entreprise

# Pour aller plus vite 
> “il faut faire du devops”
```
/mode +o dev #prod
```
> “donnons les droits admins aux devs”

# sans l'équipe d'infra
* peu de formalisation des changements
* liberté pour les développeurs :thumbsup: :thumbsdown:

# DEV/OPS
> - ![](img/two_teams.png){width=80% height=80%}

# les problèmes qui en découlent

Les développeurs :

> “chez moi, ça marche, votre conf n'est pas bonne”

Les administrateurs système :

> “notre conf est bonne, votre applicatif est buggué”

# une implémentation
![](img/three_teams.png){width=60% height=60%}

# toujours des silos
![](img/three_teams_2.png){width=60% height=60%}

> “There is no such thing as a devops team. @jezhumble”

:::notes
Creating another functional silo that sits between dev and ops is clearly a poor (and ironic) way to try and solve these problems. Devops proposes instead strategies to create better collaboration between functional silos, or doing away with the functional silos altogether and creating cross-functional teams (or some combination of these approaches).
:::

# DEV = PROD
## la solution miracle

> - On va faire du docker

> - On s'éloigne encore plus du contexte de production

#    
![Alan Cox](img/alan_cox.jpg){width=30% height=30%}

:::notes
considéré comme le développeur linux-kernel après linus T
:::

# Le mécanolanger

<span style="font-size: 20px"> comme devops, c'est un *mot valise*</span>

> - ![](img/mecanolanger_product.png)
> - qui fabrique du pain au gout d'huile…

# Beyond devops

> - SRE - Site Reliability Engineering :moneybag:
> - NoOps :smile:
> - DevSecOps :scream:
> - BizDevSecOps :joy:

:::notes
NoOps (No Operations) – l'absence de personnel d'exploitation – est un concept selon lequel un environnement informatique a atteint un niveau d'automatisation et de virtualisation suffisant, par rapport à son infrastructure sous-jacente, pour que plus aucune équipe interne ne soit nécessaire à l'administration de l’infrastructure logicielle.
:::

# Le bon devops
![](img/mouton.jpg){width=40% height=40%}

> - mouton à 5 pattes

# Break

:::notes
à partir de là, la présentation va être plus sérieuse, voire austère
:::


# C'est quoi devops ? {data-background="img/devops.png"}

# Retour aux sources
![](img/from_agile_manifesto.png){width=80% height=80%}

# Manifeste agile

![](img/agile_manifesto_stabilo.png){width=100% height=100%}

#

![](img/agile_manifesto_principles1.png){width=80% height=80%}

#

![](img/agile_manifesto_principles2.png){width=80% height=80%}

# Méthodes agiles

* XP teams are self-organizing and cross-functional.
* Scrum Teams are self-organizing and cross-functional.

# From Concept To Cash

> “How long would it take your organization to deploy a change
that involves just one single line of code?”

Mary and Tom Poppendieck

# Les origines
Patrick Debois intervient régulièrement dans un groupe discussion

<span style="background:rgba(0,0,0, 0.5);">groups.google.com/agile-system-administration</span>

> - et organise à Genth (Belgique) en 2009

# {data-background="img/devops_days09.png"}

<span style="background:rgba(0, 0, 0, 0.5);">the conference that tries to get the best of both dev and ops world.</span>

:::notes
So if you are a developer with a interest for system administration.
or a sysadmin interested in development.
:::

# Quotes

> “I'll be honest, for the past few years, when I went to some of the Agile conferences, it felt like preaching in the dessert. I was kinda giving up, maybe the idea was too crazy: developers and ops working together.”

Patrick Debois

^http://www.jedi.be/blog/2009/11/15/devopsdays09-two-weeks-later/^

# Les piliers du devops
> - Culture
> - Automation
> - (Lean)
> - Measurement
> - Sharing

:::notes
John Willis & Damon Edwards

Culture : de la collaboration Individus pour un nouvel modèle opérationnel 
viser des optimations globales et non plus locale - responsabilisation, délégation, 
autonomie, confiance
Automation : aller au délà de l'integration continue, infrastructure as code, de continous delivery au deploiement continu,  chatops, API
Lean from James Shore- les pratiques : kaizen , éliminer les gaspillages, le Value Stream Mapping
Measurement : corréler les métriques métiers avec les métriques techniques
Sharing: Vision commune Problèmes communs Glossaire commun Connaissance commune Outils communs
:::
# L'équipe produit

> “You build it, you run it” 

# People over Process over Tools

----------  -------------------------------
Individus   Va-t-on le faire ? 
Processus   Doit-on le faire ?
Tools       Peut-on technique le faire ?
----------  -------------------------------


:::notes
Individus plus que le processus plus que les outils
des couches/layers successives
:::

# Questions ?
![](img/everybodydevopsing.jpg){width=70% height=70%}

# Quelques liens

* [Blog de Patrick Debois](http://www.jedi.be/)
* [The agile admin](https://theagileadmin.com/what-is-devops)
* [devops manifesto](https://theagileadmin.com/2010/10/15/a-devops-manifesto)
* [devops culture](https://itrevolution.com/devops-culture-part-1)
* [devopsdays 2009 - dev meets ops, ops meet dev](https://www.youtube.com/watch?v=EOveXZhJpr4)
* [People over Process over Tools](http://dev2ops.org/2010/02/people-over-process-over-tools)
* [There is no such thing as a devops team](https://www.thoughtworks.com/insights/blog/there-no-such-thing-devops-team) by jezhumble

#  

![](img/url_presentation.png){width=40% height=40%}

* [Présentation](https://xtrmz.gitlab.io/devoops/index.html)
* [Source](https://gitlab.com/xtrmz/devoops)

# Annexes : Les perles 
* Tu es expert des langages Python, Ruby, Go, Bash, JavaScript et PHP
* Tu maîtrises l’administration de machine Linux

# Annexes : Les perles 
## Compétences techniques

> Le DevOps doit savoir programmer. 

