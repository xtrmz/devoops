# Présentation

* [Présentation](https://xtrmz.gitlab.io/devoops/index.html)
* [Source](https://gitlab.com/xtrmz/devoops)

Pour la génération du wordcloud utilisation de https://www.nuagesdemots.fr/

# Génération en local
docker run -v `pwd`:/source conoria/alpine-pandoc pandoc --metadata-file=/source/index.yaml --standalone -V transition=slide -V revealjs-url=https://revealjs.com/ -t revealjs -f markdown+smart+emoji /source/index.md --include-in-header=/source/css/adjust.css -o /source/index.html

